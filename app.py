from flask import Flask, render_template, request
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.cluster.hierarchy import linkage, fcluster
from collections import Counter
import random

'''
    Text clusterization task
    Input: texts separated by commas
    Output: the amount of texts in the most popular cluster
'''

app = Flask(__name__)


@app.errorhandler(Exception)
def fallback_solution(e):
    max_texts_cluster = 1
    return render_template('fallback.html', max_texts_cluster=max_texts_cluster)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        try:
            input_texts = str(request.form['input_texts']).split(',')
            input_texts = [x.strip() for x in input_texts]
            
            tf_idf = TfidfVectorizer(ngram_range=(1, 2))
            tf_idf_matrix = tf_idf.fit_transform(input_texts)
            
            Z = linkage(tf_idf_matrix.toarray(), method='ward', metric='euclidean')
            clusters = fcluster(Z, t=1, criterion='distance')
            
            c = Counter(clusters)
            max_texts_cluster = list({k: v for k, v in sorted(c.items(), key=lambda item: item[1], reverse=True)}.values())[0]

            return render_template('index.html', max_texts_cluster=max_texts_cluster, input_texts=input_texts)
        except ValueError:
            return render_template('index.html', error="Please, enter texts separated by commas!")
    
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, port=8080)
